
![](https://img.shields.io/badge/build-Success-green.svg) ![](https://img.shields.io/badge/script-BASH-blue.svg)
# Custom-Update
Automatic update > upgrade > cleanup using Debian's APT package manager.

## Useage:
```update``` ... thats it

## Notes
Please make sure that `/usr/local/bin/` is in your PATH variable if you wish to install this
script globally. Otherwise, change all path values to the directory you want.

## Setup/Install
This will install the program in the `/usr/local/bin/` directory 
```
git clone git@gitlab.com:nickpop/CustomUpdate.git;
cd CustomUpdate;
make;
```

## Uninstall
This will uninstall the program in the `/usr/local/bin/` directory.
```
make uninstall
```

## Future Considerations
- Extent this automation to other package managers like Yaourt, Yum ... etc
- Detect the OS and decide which package manager to use for the automation